;;; init.el for deadpan-emacs-config
;; author: micha ebert-hanke

;; ================================
;; basics to start with
;; ================================

;; separate custom stuff to separate file
(setq custom-file (concat user-emacs-directory "custom.el"))
(when (file-exists-p custom-file)
  (load custom-file))

;; cl is deprecated, we know ...
;; fixes a warning
(setq byte-compile-warnings '(cl-functions))

;; os specific setup
;; fixing keys and dired ls
(when (string= system-type "darwin")       
  (set-keyboard-coding-system nil)
  (setq mac-option-key-is-meta nil
	    mac-command-key-is-meta t
	    mac-command-modifier 'meta
	    mac-option-modifier 'none)
  (setq ns-function-modifier 'super)
  (setq dired-use-ls-dired nil))

;; ================================
;; define and install packages 
;; ================================

;; package archives
(require 'package)
(add-to-list 'package-archives '("gnu"   . "https://elpa.gnu.org/packages/"))
(add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/"))
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
(add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/"))

;; list of all packages to install
(setq package-list
      '(
        auctex
        consult
        corfu
        delight
        eglot
        embark
        embark-consult
        emmet-mode
        evil
        evil-collection
        evil-escape
        exec-path-from-shell
        guess-language
        js2-mode
        json-mode
        magit
        marginalia
        markdown-mode
        orderless
        org-autolist
        ox-reveal
        pass
        pdf-tools
        php-mode
        prettier-js
        projectile
        reftex
        rustic
        sqlformat
        scad-mode
        toml-mode
        vertico
        vterm
        web-mode
        which-key
        yaml-mode
        yasnippet
        ))

;; initialize
(package-initialize)

;; refresh
(unless package-archive-contents
  (package-refresh-contents))

;; install all packages from list if they are missing
(dolist (package package-list)
  (unless (package-installed-p package)
    (package-install package)))

;; ================================
;; some defaults
;; ================================

;; exec-path
;; if you are not in terminal your $PATH might not work inside emacs
;; you might need to adjust for your OS
(require 'exec-path-from-shell)
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize))

;; https://github.com/angrybacon/dotemacs/blob/master/dotemacs.org
(setq-default
 ad-redefinition-action 'accept         ; Silence warnings for redefinition
 auto-save-list-file-prefix nil         ; Prevent tracking for auto-saves
 cursor-in-non-selected-windows nil     ; Hide the cursor in inactive windows
 ;; cursor-type '(hbar . 2)                ; Underline-shaped cursor
 ;; custom-unlispify-menu-entries nil      ; Prefer kebab-case for titles
 ;; custom-unlispify-tag-names nil         ; Prefer kebab-case for symbols
 delete-by-moving-to-trash t            ; Delete files to trash
 fill-column 80                         ; Set width for automatic line breaks
 gc-cons-threshold (* 8 1024 1024)      ; We're not using Game Boys anymore
 help-window-select t                   ; Focus new help windows when opened
 indent-tabs-mode nil                   ; Stop using tabs to indent
 inhibit-startup-screen t               ; Disable start-up screen
 initial-scratch-message ""             ; Empty the initial *scratch* buffer
 mouse-yank-at-point t                  ; Yank at point rather than pointer
 read-process-output-max (* 1024 1024)  ; Increase read size per process
 recenter-positions '(5 top bottom)     ; Set re-centering positions
 scroll-conservatively 101              ; Avoid recentering when scrolling far
 scroll-margin 2                        ; Add a margin when scrolling vertically
 select-enable-clipboard t              ; Merge system's and Emacs' clipboard
 sentence-end-double-space nil          ; Use a single space after dots
 show-help-function nil                 ; Disable help text everywhere
 tab-always-indent 'complete            ; Tab indents first then tries completions
 tab-width 4                            ; Smaller width for tab characters
 uniquify-buffer-name-style 'forward    ; Uniquify buffer names
 warning-minimum-level :error           ; Skip warning buffers
 window-combination-resize t            ; Resize windows proportionally
 x-stretch-cursor t)                    ; Stretch cursor to the glyph width
(blink-cursor-mode 0)                   ; Prefer a still cursor
(delete-selection-mode 1)               ; Replace region when inserting text
(fset 'yes-or-no-p 'y-or-n-p)           ; Replace yes/no prompts with y/n
(global-subword-mode 1)                 ; Iterate through CamelCase words
(mouse-avoidance-mode 'exile)           ; Avoid collision of mouse with point
(put 'downcase-region 'disabled nil)    ; Enable downcase-region
(put 'upcase-region 'disabled nil)      ; Enable upcase-region
(setq completion-cycle-threshold 3)

;; no loadtime in modeline
(setq display-time-default-load-average nil)

;; visible bell / ring bell
(setq visible-bell nil)
(setq ring-bell-function 'ignore)

;; no menu-, tool- and scrollbar
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)

;; utf-8 please
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(set-file-name-coding-system 'utf-8)
(set-clipboard-coding-system 'utf-8)
(set-buffer-file-coding-system 'utf-8)
(set-language-environment 'utf-8)

;; eldoc
(global-eldoc-mode 0)

;; ================================
;; usability
;; ================================

;; delight
(require 'delight)
(delight 'evil-escape-mode nil 'evil-escape)
(delight 'subword-mode nil 'subword)
(delight 'evil-collection-unimpaired-mode nil 'evil-collection-unimpaired)
(delight 'rustic-mode "Rustic" :major)

;; visit config file
(defun config-visit ()
  (interactive)
  (find-file (concat user-emacs-directory "init.el")))
(global-set-key (kbd "C-c e") 'config-visit)

;; switch between recently used buffers
(global-set-key (kbd "M-o")  'mode-line-other-buffer)

;; navigate between windows
;; use shift-arrowkey to navigate when possible otherwise M-x o
(when (fboundp 'windmove-default-keybindings)
  (windmove-default-keybindings))

;; line numbers
(setq display-line-numbers-type 'relative) 
(add-hook 'prog-mode-hook 'display-line-numbers-mode)

;; highlight matchin parens
(show-paren-mode 1)

;; electric pair
(electric-pair-mode 1)

;; always kill current buffer
(defun kill-curr-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))
(global-set-key (kbd "C-x k") 'kill-curr-buffer)

;; prettify symbols
(global-prettify-symbols-mode t)

;; dired settings
(put 'dired-find-alternate-file 'disabled nil) ; press 'a' to reuse dired buffer
(setq dired-dwim-target t) ; dired target is other split

;; autosave
(setq auto-save-interval 20)

;; backups
(setq
 backup-by-copying t    
 kept-new-versions 10  
 kept-old-versions 0  
 delete-old-versions t 
 version-control t    
 vc-make-backup-files t)

(setq backup-directory-alist
      `(("." . ,(concat user-emacs-directory "backups"))))

;; spell checking flyspell

(if (file-exists-p "/usr/bin/hunspell")
    (progn
      (setq ispell-program-name "hunspell")
      (eval-after-load "ispell"
        '(progn (defun ispell-get-coding-system () 'utf-8)))))

(dolist (hook '(text-mode-hook))
  (add-hook hook (lambda () (flyspell-mode 1))))

;; for mouse
(eval-after-load "flyspell"
  '(progn
     (define-key flyspell-mouse-map [down-mouse-3] #'flyspell-correct-word)
     (define-key flyspell-mouse-map [mouse-3] #'undefined)))

;; spell checking guess language
;; needs proper locale in shell
;; also needs hunspell and dics
;; https://stackoverflow.com/questions/56429761/on-macos-hunspell-cant-find-existing-aff-and-dic-files-in-library-spelling

;; Default dictionary to use
(setq ispell-local-dictionary "deutsch")

;; fix for mac
;;(when (string-equal system-type "darwin") ; Only for macOs
;;   ;;Dictionary file name
;;   (setenv "DICTIONARY" "de_DE"))

;; guess-language
(require 'guess-language)
(setq guess-language-langcodes
      '((en . ("en_US" "English"))
	    (de . ("de_DE" "German"))))
(setq guess-language-languages '(en de))
(setq guess-language-min-paragraph-length 35)
(add-hook 'org-mode-hook 'guess-language-mode)
(add-hook 'text-mode-hook 'guess-language-mode)

;; elisp flymake
(add-hook 'emacs-lisp-mode-hook #'flymake-mode)

;; pinentry
(require 'epg)
(setq epg-pinentry-mode 'loopback)

;; ================================
;; completion
;; ================================

;; which-key
(require 'which-key)
(which-key-mode)

;; vertico
(require 'vertico)
(vertico-mode)
(savehist-mode 1)
(setq vertico-cycle t)
(define-key vertico-map (kbd "C-k") 'vertico-previous)
(define-key vertico-map (kbd "C-j") 'vertico-next)
(define-key vertico-map (kbd "C-f") 'vertico-exit)
;;(custom-set-faces '(vertico-current ((t (:background "#616161")))))

;; https://github.com/daviwil/dotfiles/blob/master/Emacs.org#completions-with-vertico
(defun deadpan/minibuffer-backward-kill (arg)
  "When minibuffer is completing a file name delete up to parent
folder, otherwise delete a word"
  (interactive "p")
  (if minibuffer-completing-file-name
      ;; Borrowed from https://github.com/raxod502/selectrum/issues/498#issuecomment-803283608
      (if (string-match-p "/." (minibuffer-contents))
          (zap-up-to-char (- arg) ?/)
        (delete-minibuffer-contents))
    (delete-word (- arg))))
(define-key minibuffer-local-map (kbd "C-l") 'deadpan/minibuffer-backward-kill)

;; orderless
(require 'orderless)
(setq completion-styles '(orderless)
      completion-category-defaults nil
      completion-category-overrides '((file (styles partial-completion))))

;; marginalia
(require 'marginalia)
(marginalia-mode)
(global-set-key (kbd "M-A") 'marginalia-cycle)

;; consult
(require 'consult)

(define-key (current-global-map) (kbd "C-c h") 'consult-history)
(define-key (current-global-map) (kbd "C-c m") 'consult-mode-command)
(define-key (current-global-map) (kbd "C-c b") 'consult-bookmark)
(define-key (current-global-map) (kbd "C-c k") 'consult-kmacro)

;; C-x bindings (ctl-x-map)
(define-key (current-global-map) (kbd "C-x M-:") 'consult-complex-command)     ;; orig. repeat-complex-command
(define-key (current-global-map) (kbd "C-x b") 'consult-buffer)                ;; orig. switch-to-buffer
(define-key (current-global-map) (kbd "C-x 4 b") 'consult-buffer-other-window) ;; orig. switch-to-buffer-other-window
(define-key (current-global-map) (kbd "C-x 5 b") 'consult-buffer-other-frame)  ;; orig. switch-to-buffer-other-frame

;; Custom M-# bindings for fast register access
(define-key (current-global-map) (kbd "M-#") 'consult-register-load)
(define-key (current-global-map) (kbd "M-'") 'consult-register-store)          ;; orig. abbrev-prefix-mark (unrelated)
(define-key (current-global-map) (kbd "C-M-#") 'consult-register)

;; Other custom bindings
(define-key (current-global-map) (kbd "M-y") 'consult-yank-pop)                ;; orig. yank-pop
(define-key (current-global-map) (kbd "<help> a") 'consult-apropos)            ;; orig. apropos-command

;; M-g bindings (goto-map)
(define-key (current-global-map) (kbd "M-g e") 'consult-compile-error)
(define-key (current-global-map) (kbd "M-g f") 'consult-flymake)               ;; Alternative: consult-flycheck
(define-key (current-global-map) (kbd "M-g g") 'consult-goto-line)             ;; orig. goto-line
(define-key (current-global-map) (kbd "M-g M-g") 'consult-goto-line)           ;; orig. goto-line
(define-key (current-global-map) (kbd "M-g o") 'consult-outline)               ;; Alternative: consult-org-heading
(define-key (current-global-map) (kbd "M-g m") 'consult-mark)
(define-key (current-global-map) (kbd "M-g k") 'consult-global-mark)
(define-key (current-global-map) (kbd "M-g i") 'consult-imenu)
(define-key (current-global-map) (kbd "M-g I") 'consult-imenu-multi)

;; M-s bindings (search-map)
(define-key (current-global-map) (kbd "M-s f") 'consult-find)
(define-key (current-global-map) (kbd "M-s F") 'consult-locate)
(define-key (current-global-map) (kbd "M-s g") 'consult-grep)
(define-key (current-global-map) (kbd "M-s G") 'consult-git-grep)
(define-key (current-global-map) (kbd "M-s r") 'consult-ripgrep)
(define-key (current-global-map) (kbd "C-s") 'consult-line)
(define-key (current-global-map) (kbd "M-s L") 'consult-line-multi)
(define-key (current-global-map) (kbd "M-s m") 'consult-multi-occur)
(define-key (current-global-map) (kbd "M-s k") 'consult-keep-lines)
(define-key (current-global-map) (kbd "M-s u") 'consult-focus-lines)

;; Isearch integration
(define-key (current-global-map) (kbd "M-s e") 'consult-isearch-history)
(define-key isearch-mode-map (kbd "M-e") 'consult-isearch-history)         ;; orig. isearch-edit-string
(define-key isearch-mode-map (kbd "M-s e") 'consult-isearch-history)       ;; orig. isearch-edit-string
(define-key isearch-mode-map (kbd "M-s l") 'consult-line)                  ;; needed by consult-line to detect isearch
(define-key isearch-mode-map (kbd "M-s L") 'consult-line-multi)           ;; needed by consult-line to detect isearch

;; Optionally configure the register formatting. This improves the register
;; preview for `consult-register', `consult-register-load',
;; `consult-register-store' and the Emacs built-ins.
(setq register-preview-delay 0
      register-preview-function #'consult-register-format)

;; Optionally tweak the register preview window.
;; This adds thin lines, sorting and hides the mode line of the window.
(advice-add #'register-preview :override #'consult-register-window)

;; Optionally replace `completing-read-multiple' with an enhanced version.
(advice-add #'completing-read-multiple :override #'consult-completing-read-multiple)

;; Use Consult to select xref locations with preview
(setq xref-show-xrefs-function #'consult-xref
      xref-show-definitions-function #'consult-xref)

;; Configure other variables and modes in the :config section,
;; after lazily loading the package.
(with-eval-after-load 'consult
  ;; Optionally configure preview. The default value
  ;; is 'any, such that any key triggers the preview.
  ;; (setq consult-preview-key 'any)
  ;; (setq consult-preview-key (kbd "M-."))
  ;; (setq consult-preview-key (list (kbd "<S-down>") (kbd "<S-up>")))
  ;; For some commands and buffer sources it is useful to configure the
  ;; :preview-key on a per-command basis using the `consult-customize' macro.

  (consult-customize
   consult-ripgrep consult-git-grep consult-grep
   consult-bookmark consult-recent-file consult-xref
   consult--source-bookmark consult--source-recent-file
   consult--source-project-recent-file)

  ;; Preview on any key press, but delay 0.5s
  (consult-customize consult-theme :preview-key '(:debounce 0.2 any))

  ;; old block removed consult seems to have changed
  ;;  (consult-customize
  ;;   consult-theme
  ;;   :preview-key '(:debounce 0.2 any)
  ;;   consult-ripgrep consult-git-grep consult-grep
  ;;   consult-bookmark consult-recent-file consult-xref
  ;;   consult--source-file consult--source-project-file consult--source-bookmark
  ;;   :preview-key (kbd "M-."))

  ;; Optionally configure the narrowing key.
  ;; Both < and C-+ work reasonably well.
  (setq consult-narrow-key "<") ;; (kbd "C-+")

  ;; Optionally make narrowing help available in the minibuffer.
  ;; You may want to use `embark-prefix-help-command' or which-key instead.
  ;; (define-key consult-narrow-map (vconcat consult-narrow-key "?") #'consult-narrow-help)

  ;; Optionally configure a function which returns the project root directory.
  ;; There are multiple reasonable alternatives to chose from.
  ;; 2. projectile.el (projectile-project-root)
  (autoload 'projectile-project-root "projectile")
  (setq consult-project-root-function #'projectile-project-root))

;; Use `consult-completion-in-region' if Vertico is enabled.
;; Otherwise use the default `completion--in-region' function.
(setq completion-in-region-function
      (lambda (&rest args)
        (apply (if vertico-mode
                   #'consult-completion-in-region
                 #'completion--in-region)
               args)))

;; embark
(require 'embark)
(define-key (current-global-map) (kbd "C-;") 'embark-act)
(define-key (current-global-map) (kbd "C-'") 'embark-dwim)
;; (define-key (current-global-map) (kbd "C-h B") 'embark-bindings)

;; Optionally replace the key help with a completing-read interface
(setq prefix-help-command #'embark-prefix-help-command)

;; Hide The Mode Line Of The Embark Live/Completions Buffers
(add-to-list 'display-buffer-alist
             '("\\`\\*embark collect \\(live\\|completions\\)\\*"
               nil
               (window-parameters (mode-line-format . none))))

;; https://github.com/oantolin/embark/wiki/Additional-Configuration#use-which-key-like-a-key-menu-prompt
(defun embark-which-key-indicator ()
  "An embark indicator that displays keymaps using which-key.
The which-key help message will show the type and value of the
current target followed by an ellipsis if there are further
targets."
  (lambda (&optional keymap targets prefix)
    (if (null keymap)
        (which-key--hide-popup-ignore-command)
      (which-key--show-keymap
       (if (eq (plist-get (car targets) :type) 'embark-become)
           "Become"
         (format "Act on %s '%s'%s"
                 (plist-get (car targets) :type)
                 (embark--truncate-target (plist-get (car targets) :target))
                 (if (cdr targets) "…" "")))
       (if prefix
           (pcase (lookup-key keymap prefix 'accept-default)
             ((and (pred keymapp) km) km)
             (_ (key-binding prefix 'accept-default)))
         keymap)
       nil nil t (lambda (binding)
                   (not (string-suffix-p "-argument" (cdr binding))))))))

(setq embark-indicators
      '(embark-which-key-indicator
        embark-highlight-indicator
        embark-isearch-highlight-indicator))

(defun embark-hide-which-key-indicator (fn &rest args)
  "Hide the which-key indicator immediately when using the completing-read prompter."
  (which-key--hide-popup-ignore-command)
  (let ((embark-indicators
         (remq #'embark-which-key-indicator embark-indicators)))
    (apply fn args)))

(advice-add #'embark-completing-read-prompter
            :around #'embark-hide-which-key-indicator)

;; embark-consult
(require 'embark-consult)
(with-eval-after-load (progn 'embark 'consult)
  (add-hook 'embark-collect-mode-hook 'consult-preview-at-point-mode))

;; corfu
(require 'corfu)
(global-corfu-mode)
(define-key corfu-map (kbd "C-j") 'corfu-next)
(define-key corfu-map (kbd "C-k") 'corfu-previous)

;; yasnippet
;; some nice tutorial: https://jdhao.github.io/2021/10/06/yasnippet_setup_emacs/
(require 'yasnippet)
(define-key yas-minor-mode-map (kbd "M-z") 'yas-expand)
(yas-reload-all)
(add-hook 'markdown-mode-hook #'yas-minor-mode)
(add-hook 'dp-vue-mode-hook #'yas-minor-mode)

;; ================================
;; beauty
;; ================================

;; font
;; i recommend using a nerd font like SauceCodePro Nerd Font
;; https://www.nerdfonts.com
;; setting it for window mode emacs
(when (display-graphic-p)
  (when (member "SauceCodePro Nerd Font" (font-family-list))
    (set-frame-font "SauceCodePro Nerd Font-12" t t)))

(defun increase-fontsize()
  (interactive)
(when (display-graphic-p)
  (when (member "SauceCodePro Nerd Font" (font-family-list))
    (set-frame-font "SauceCodePro Nerd Font-16" t t)))
)

(global-set-key (kbd "C-c f") 'increase-fontsize)

;; fringes
(setq fringe-mode 'minimal)

;; font-lock
;; no font-lock in general
(global-font-lock-mode 0)
;; but for some things ...
(add-hook 'diff-mode-hook 'turn-on-font-lock) ; diff
(add-hook 'dired-mode-hook 'turn-on-font-lock) ; dired
(add-hook 'magit-mode-hook 'turn-on-font-lock) ; magit
(add-hook 'vterm-mode-hook 'turn-on-font-lock) ; vterm
(add-hook 'org-mode-hook 'turn-on-font-lock) ; org
(add-hook 'markdown-mode-hook 'turn-on-font-lock) ; markdown
(add-hook 'yaml-mode-hook 'turn-on-font-lock) ; yaml
(add-hook 'notmuch-mode-hook 'turn-on-font-lock) ; notmuch
(add-hook 'LaTeX-mode-hook 'turn-on-font-lock) ; LaTeX

;; theme

;; Add all your customizations prior to loading the themes
(setq modus-themes-italic-constructs t
      modus-themes-bold-constructs nil
      modus-themes-region '(bg-only no-extend))

;; Load the theme files before enabling a theme
(load-theme 'modus-vivendi)             ; Dark theme
(load-theme 'modus-operandi)            ; Light theme

;; Load the theme of your choice:
(define-key global-map (kbd "<f5>") #'modus-themes-toggle)

;; ;; ================================
;; ;; email notmuch
;; ;; ================================

;; ;; getting mail with isync
;; ;; https://wiki.archlinux.org/title/Isync

;; ;; sending mail with msmtp
;; ;; https://wiki.archlinux.org/title/Msmtp

;; ;; notmuch has to be installed first
;; (require 'notmuch)
;; (global-set-key (kbd "C-c M") 'notmuch)

;; ;; this is set relative to your main maildir, where the .notmuch lives
;; (setq message-directory "test/drafts")
;; ;; here you set the directory for the sent mail and the taglist for sent mail
;; (setq notmuch-fcc-dirs "test/sent -unread -inbox +sent")
;; ;; add Cc and Bcc headers to the message buffer
;; (setq message-default-mail-headers "Cc: \nBcc: \n")
;; ;; use msmtp to send mail
;; (setq message-send-mail-function 'message-send-mail-with-sendmail)
;; ;; look up path with "which"
;; (setq sendmail-program "/opt/homebrew/bin/msmtp")
;; ;; tell msmtp to choose the SMTP server according to the from field in the outgoing email - taken from Adolfo Villafiorita
;; (setq message-sendmail-extra-arguments '("--read-envelope-from"))
;; (setq message-sendmail-f-is-evil 'T)
;; ;; close buffer after send  
;; (setq message-kill-buffer-on-exit t)

;; ;; notmuch "mailboxes" (predefined searches)

;; (setq notmuch-saved-searches 
;;       '((:name "Private"
;;                :query "tag:private AND tag:unread"
;;                :key "p"
;;                :sort-order newest-first
;;                :search-type 'tree)
;;         (:name "Job"
;;                :query "tag:job AND tag:unread AND NOT tag:feed"
;;                :key "j"
;;                :sort-order newest-first
;;                :search-type 'tree)
;;         (:name "Feed"
;;                :query "tag:feed AND tag:unread"
;;                :key "f"
;;                :sort-order newest-first)
;;         (:name "Verwaltung"
;;                :query "tag:verwaltung AND tag:unread"
;;                :key "v"
;;                :sort-order newest-first
;;                :search-type 'tree)
;;         (:name "Old 00a9"
;;                :query "path:00a9/**"
;;                :key "X"
;;                :sort-order newest-first
;;                :search-type 'tree)
;;         (:name "Old graum"
;;                :query "path:graum/**"
;;                :key "Y"
;;                :sort-order newest-first
;;                :search-type 'tree)
;;         (:name "To Reply"
;;                :query "tag:toreply AND NOT tag:unread"
;;                :key "r"
;;                :search-type 'tree)
;;         (:name "All"
;;                :query "*"
;;                :key "a")
;;         (:name "Archive"
;;                :query "tag:archive"
;;                :key "A")))

;; ================================
;; config for packages
;; ================================

;; sqlformat
;; pgformatter needs to be installed
;; https://github.com/darold/pgFormatter
(setq sqlformat-command 'pgformatter)
;; Optional additional args
(setq sqlformat-args '("-s2" "-g"))
(require 'sqlformat)
(add-hook 'sql-mode-hook 'sqlformat-on-save-mode)

;; pass
(require 'pass)

;; evil

(setq evil-want-keybinding nil)
(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
(setq evil-want-C-i-jump nil) ; fix tab, must be before require evil
(require 'evil)
(evil-mode 1)
(when (require 'evil-collection nil t)
  (evil-collection-init))

;; unset "C-k" in evil insert, fixes binding for corfu
(define-key evil-insert-state-map (kbd "C-k") nil)

;; change the evil cursor depending on state in emacs -nw
;; https://www2.ccs.neu.edu/research/gpc/VonaUtils/vona/terminal/vtansi.htm
;; https://github.com/h0d/term-cursor.el/blob/master/term-cursor.el
;; 0, 2- box
;; 1 - blinking box
;; 3 - blinking underline
;; 4 - steady underline
;; 5 - blinking bar
;; 6 - steady bar
(unless (display-graphic-p)
  (add-hook 'evil-insert-state-entry-hook (lambda () (send-string-to-terminal "\033[5 q")))
  (add-hook 'evil-normal-state-entry-hook (lambda () (send-string-to-terminal "\033[0 q")))
  )

;; evil-escape
(with-eval-after-load 'evil
  (require 'evil-escape)
  (evil-escape-mode 1)
  (setq-default evil-escape-key-sequence "ii")
  (setq-default evil-escape-delay 0.4))

;; vterm
(with-eval-after-load 'vterm
  (setq vterm-max-scrollback 10000)
  (advice-add 'evil-collection-vterm-insert :before #'vterm-reset-cursor-point))

(setq vterm-buffer-name-string "vterm %s") ; set the buffer to working directory

;; function for fish shell
;; # set vterm title via fish
;; function fish_title
;;   #  hostname
;;   #  echo ":"
;;     pwd
;; end

(global-set-key (kbd "C-c t") 'vterm)

;; multi-vterm
;; possible config: https://github.com/suonlight/multi-vterm
;;(require 'multi-vterm)

;; projectile
(require 'projectile)
(projectile-mode +1)
;; modeline
(setq projectile-mode-line-function (lambda () (format " Proj-[%s]" (projectile-project-name))))
;; Recommended keymap prefix on macOS
;; (define-key projectile-mode-map (kbd "s-p") 'projectile-command-map)
;; Recommended keymap prefix on Windows/Linux
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

;; eglot
;; install required LSP servers:
;; - Rust
;; $ git clone https://github.com/rust-analyzer/rust-analyzer.git
;; $ cd rust-analyzer
;; $ cargo xtask install --server # will install rust-analyzer into $HOME/.cargo/bin
;; - Vue
;; $ npm install -g vls
;; - Html, CSS
;; $ npm install -g vscode-langservers-extracted
(require 'eglot)
(define-key eglot-mode-map (kbd "C-c r") 'eglot-rename)
(define-key eglot-mode-map (kbd "C-c o") 'eglot-code-action-organize-imports)
(define-key eglot-mode-map (kbd "C-c h") 'eldoc)
(define-key eglot-mode-map (kbd "<f6>") 'xref-find-definitions)
;; eglot for vue
;; eglot needs to know the language id
;; some resource on vue and eglot: https://genehack.blog/2020/08/web-mode-eglot-vetur-vuejs-=-happy/
;; see here: https://github.com/joaotavora/eglot/issues/525#issuecomment-678540557
;; (put 'web-mode 'eglot-language-id "vue")
(put 'dp-vue-mode 'eglot-language-id "vue")
(add-to-list 'eglot-server-programs '(dp-vue-mode . ("vls")))
(add-to-list 'eglot-server-programs '(css-mode . ("vscode-css-language-server" "--stdio")))
;;(add-to-list 'eglot-server-programs '(web-mode . ("vscode-html-language-server" "--stdio")))
(add-hook 'dp-vue-mode-hook 'eglot-ensure)
;;(add-hook 'web-mode-hook 'eglot-ensure)
(add-hook 'css-mode-hook 'eglot-ensure)
;; eglot for LaTeX
;; https://github.com/latex-lsp/texlab
(add-to-list 'eglot-server-programs '(latex-mode . ("texlab")))
(add-hook 'LaTeX-mode-hook 'eglot-ensure)


;; rustic    
(setq rustic-lsp-client 'eglot)
(require 'rustic)
;;(add-hook 'eglot--managed-mode-hook (lambda () (eldoc-mode 0)))
;;(add-hook 'eglot--managed-mode-hook (lambda () (flymake-mode -1)))
;;(push 'rustic-clippy flycheck-checkers)
;;(setq rustic-flycheck-clippy-params "--message-format=json")
(setq rustic-format-on-save t) ; disable if autoformat is not wanted

;; markdown-mode
(require 'markdown-mode)
(autoload 'markdown-mode "markdown-mode"
  "Major mode for editing Markdown files" t)
(add-to-list 'auto-mode-alist
             '("\\.\\(?:md\\|markdown\\|mkd\\|mdown\\|mkdn\\|mdwn\\)\\'" . markdown-mode))
(add-hook 'markdown-mode 'visual-line-mode)

(autoload 'gfm-mode "markdown-mode"
  "Major mode for editing GitHub Flavored Markdown files" t)
(add-to-list 'auto-mode-alist '("README\\.md\\'" . gfm-mode))
(add-hook 'gfm-mode 'visual-line-mode)

;; org mode settings

;; org-agenda files
(setq org-agenda-files '("~/orgfiles"))

;; general settings
(define-key global-map "\C-cl" 'org-store-link)
(define-key global-map "\C-ca" 'org-agenda)
(setq org-log-done t)
;; soft wrap lines and indent for org mode
(with-eval-after-load 'org       
  (setq org-startup-indented t) 
  (add-hook 'org-mode-hook 'visual-line-mode)
  (add-hook 'org-mode-hook 'flyspell-mode))
;; adjust time format for clocksum in column view
(setq org-duration-format 'h:mm)
(setq org-time-clocksum-format (quote (:hours "%d" :require-hours t :minutes ":%02d" :require-minutes t)))
;; clock into drawer CLOCKING
(setq org-clock-into-drawer "CLOCKING")

;; org-autolist
(require 'org-autolist)
(add-hook 'org-mode-hook 'org-autolist-mode)

;; prettier-js
;; prettier must be installed with `npm install -g prettier`
(require 'prettier-js)
(setq prettier-js-args '(
			             "--single-quote" "true"
			             "--prose-wrap" "never"
			             "--trailing-comma" "none"
			             ))
(add-hook 'js2-mode-hook 'prettier-js-mode)
(add-hook 'dp-vue-mode-hook 'prettier-js-mode)
;;(add-hook 'web-mode-hook 'prettier-js-mode)
;;(add-hook 'sass-mode-hook 'prettier-js-mode)

;; toml-mode
(require 'toml-mode)

;; json-mode
(require 'json-mode)

;; highlight-indent-guides-mode
;; (require 'highlight-indent-guides-mode)
;; (add-to-list 'auto-mode-alist '("\\.ya?ml\\'" . highlight-indent-guides-mode))

;; yaml-mode
(require 'yaml-mode)
(add-to-list 'auto-mode-alist
             '("\\.ya?ml\\'" . yaml-mode))
(add-hook 'yaml-mode-hook
	      (lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))

;; css-mode
(setq css-indent-offset 2)
(add-to-list 'auto-mode-alist
             '("\\.scss\\'" . css-mode))

;; ;; sass-mode
;; (require 'sass-mode)
;; (add-to-list 'auto-mode-alist
;;               '("\\.scss\\'" . sass-mode))
;; ;; (add-hook 'dp-vue-mode-hook 'sass-mode)

;; ;; scss-mode
;; (require 'scss-mode)
;; (setq scss-compile-at-save 'nil)
;; (add-to-list 'auto-mode-alist
;;              '("\\.scss\\'" . scss-mode))
;; (add-hook 'dp-vue-mode-hook 'scss-mode)

;; js2-mode
(require 'js2-mode)
(add-hook 'js2-mode-hook #'js2-imenu-extras-mode)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(setq js2-basic-offset 2)
(setq indent-tabs-mode nil)

;; emmet-mode
(require 'emmet-mode)
(add-hook 'web-mode-hook 'emmet-mode)
(add-hook 'sgml-mode-hook 'emmet-mode)

;; web-mode
(require 'web-mode)

(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)

(add-to-list 'auto-mode-alist '("\\.erb\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.mustache\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html?\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
;;(add-to-list 'auto-mode-alist '("\\.scss\\'" . web-mode))

;; define a mode for Vue
(define-derived-mode dp-vue-mode web-mode "Custom VUE mode"
  "A custom Mode to edit VUE files."
  )
(provide 'dp-vue-mode)
(add-to-list 'auto-mode-alist '("\\.vue\\'" . dp-vue-mode))

;; org-reveal
(setq org-reveal-root "/home/micha/.nvm/versions/node/v18.12.0/lib/node_modules/reveal.js")

;; pdf-tools
(pdf-loader-install) ; On demand loading, leads to faster startup time

;; latex
;; make sure latex is installed
;; sudo dnf install texlive-scheme-full
;; (latex-preview-pane-enable)
(setq +latex-viewers '(pdf-tools))
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq-default TeX-master nil)
(setq TeX-PDF-mode t)
;; Use pdf-tools to open PDF files
(setq TeX-view-program-selection '((output-pdf "PDF Tools"))
      TeX-source-correlate-start-server t)

;; Update PDF buffers after successful LaTeX runs
(add-hook 'TeX-after-compilation-finished-functions
           #'TeX-revert-document-buffer)
;; end of file
